var fs = require('fs');
var Telemetry = require('./telemetry');
var Other = require('./other');

function Logger() {
  this._now = new Date();
  this._logFile = 'logs/log_'
    + Other.padLeft(this._now.getDate(), 2) + '.'
    + Other.padLeft(this._now.getMonth() + 1, 2) + '.'
    + this._now.getFullYear()
    + '-'
    + Other.padLeft(this._now.getHours(), 2)
    + Other.padLeft(this._now.getMinutes(), 2)
    + Other.padLeft(this._now.getSeconds(), 2)
    + '.csv';
  this._startedLog = false;
  this._writeToFile = false;
  this._readings = [];

  this.start = function (readingsToWrite, writeToFile) {
    if (!this._startedLog) {
      var strToWrite = '',
        strToScreen = '';
      this._readings = readingsToWrite;
      this._writeToFile = writeToFile || false;

      if (this._writeToFile) {
        console.log('Start writing log to ' + (Other.useColors ? this._logFile.green : this._logFile));
      }

      this._readings.forEach(function (reading) {
        strToWrite += reading + ';';
        strToScreen += (Other.useColors ? reading.green : reading) + ';';
      });
      console.log('Format: ' + strToScreen);
      if (this._writeToFile) {
        fs.appendFileSync(this._logFile, strToWrite + '\n');
      }
      this._startedLog = true;
    }
  };

  /**
   * Записать данные телеметрии в лог
   *
   * @param data Массив с полями, которые надо записать в лог
   */
  this.write = function (data) {
    var strToWrite = '',
      strToScreen = '';

    this._readings.forEach(function (reading) {
      var parts = reading.split('.');
      var obj = data;
      parts.forEach(function (p) {
        obj = (typeof obj === 'undefined') ? undefined : obj[p];
      });
      if (typeof obj === 'undefined') {
        obj = '-';
      }
      strToScreen += (Other.useColors ? obj.toString().green : obj.toString()) + ';';
      strToWrite += obj.toString() + ';';
    });
    console.log(strToScreen);
    if (this._writeToFile) {
      fs.appendFileSync(this._logFile, strToWrite + '\n');
    }
  };
}

module.exports = new Logger();