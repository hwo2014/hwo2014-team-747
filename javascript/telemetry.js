var Track = require('./track');
var Const = require('./const');
var Logger = require('./logger');
var Other = require('./other');

/**
 * Вся возможная телеметрия
 */
function Telemetry() {
  this._maxTick = 0;
  this._history = [];
  this._startTickLag = -1; // Часто гонка начинается не с первого и даже не со второго тика.
  this._ourIndex = 0; // Индекс нашей машины в гонке - //todo: определять по нику в инфе
  this._turbo = false;

  this.getCurrentTick = function () {
    return this._maxTick;
  };

  this.init = function () {
    this._maxTick = 0;
    this._history = [];
  };

  this.up = function (carPositions, gameTick) {
    for (i = 0; i < carPositions.length; i++) {
      if (carPositions[i].id.color == Const.ourCar.color)
        this._ourIndex = i;
    }

//    this._maxTick = Math.max(this._maxTick, gameTick);
    this._maxTick = gameTick; //нука нахер
    var t = carPositions[this._ourIndex]; // Пока берем только свои данные

    t.gameTick = gameTick;
    t.prev = this._history[gameTick - 1];

    t.switchingNow = t.piecePosition.lane.startLaneIndex !== t.piecePosition.lane.endLaneIndex;
    t.speed = this.calcSpeed(carPositions[this._ourIndex]);
    t.acc = this.calcAcc(t);
    t.angleSpeed = this.calcAngleSpeed(t);
    t.angleAcc = this.calcAngleAcc(t);

    if (0 && t.prev) {
      var d = t.angleAcc - t.prev.next.angleAcc;
      if (Math.abs(d) > 0.01) {
        var r = Math.min(
          Track.getBendRadius(t.prev.piecePosition.pieceIndex, t.prev.piecePosition.lane.startLaneIndex),
          Track.getBendRadius(t.prev.piecePosition.pieceIndex, t.prev.piecePosition.lane.endLaneIndex)
        );

        var MAGIC_CONST_FOR_OUR_RADIUS = Const.MAGIC_CONST_FOR_RADIUS_110 * Math.sqrt(110 / r);
        console.log('FRICTION ERROR:', Const.MAGIC_CONST_MAYBE_FRICTION, 'd:', d);

        Const.MAGIC_CONST_MAYBE_FRICTION = (MAGIC_CONST_FOR_OUR_RADIUS * t.prev.speed * t.prev.speed + d) / t.prev.speed;
        console.log('NEW FR:', Const.MAGIC_CONST_MAYBE_FRICTION);
        if (Const.MAGIC_CONST_MAYBE_FRICTION < 0.1) Const.MAGIC_CONST_MAYBE_FRICTION = 0.1;
      }
    }

    t.predictedAngleAcc = this.predictAngleAcc(t);

    t.next = this.predictPosition(t, 1.0);

    if (gameTick && t.speed === 0) {
      this._startTickLag++;
    }

    this._history.push(t);
    Logger.write(t);
    if (gameTick && t.speed !== 0) {
      Const.tryToObtain(this._history, gameTick); // _maxTick коррелирует с индексами в истории. Либо gameTick+lag
    }
  };

  this.registerThrottle = function (throttle) {
    var t = this._history[this._maxTick];
    if (typeof throttle === 'undefined') {
      t.throttle = t.prev.throttle;
      t.predictedAcc = this.predictAcc(t, t.throttle);
    } else {
      t.throttle = throttle;
      t.predictedAcc = this.predictAcc(t, throttle);
    }
  };

  this.getCurrentPosition = function () {
    return this._history[this._maxTick];
  };

  this.upTurbo = function (turboInfo) {
    this._turbo = turboInfo;
    console.log('# WE GOT Turbo! ');
  };

  this.downTurbo = function () { // Обновляем данные по остаткам нитры
    this._turbo.turboDurationTicks--;
    if (this._turbo.turboDurationTicks == 0) { // Нитра кончилась
      this._turbo = false; // Нитры больше нет
      console.log('# WE USED Whole Turbo! ');
    }
  };

  this.calcSpeed = function (t) {
    if (this._maxTick < 1 || t.gameTick < 1) return 0;

    if (typeof t.prev == "undefined") {
      console.log('AAAA!');
      console.log(t);
      return 6.2;
    }

    if (t.prev.switchingNow && !t.switchingNow) {
      return t.prev.speed + t.prev.predictedAcc;
    }
    return Track.getDistanceBetweenPoints(t.prev.piecePosition, t.piecePosition);
  }

  this.calcAcc = function (t) {
    if (this._maxTick < 1 || t.gameTick < 1) return 0;
    return t.speed - t.prev.speed;
  }

  /**
   * acc (t+1) = throttle (t) * power - speed (t) * airDrag
   * @param t
   * @result float | false
   */
  this.predictAcc = function (t, throttle) {
    if (!Const.power || !Const.airDrag) {
      return false;
    }
    return throttle * Const.power + t.speed * Const.airDrag;
  }

  this.calcAngleSpeed = function (t) {
    if (this._maxTick < 1 || t.gameTick < 1) return 0;
    return t.angle - t.prev.angle;
  }

  this.calcAngleAcc = function (t) {
    if (this._maxTick < 1 || t.gameTick < 1) return 0;
    return t.angleSpeed - t.prev.angleSpeed;
  }

  this.predictAngleAcc = function (t) {
    var r = Math.min(
      Track.getBendRadius(t.piecePosition.pieceIndex, t.piecePosition.lane.startLaneIndex),
      Track.getBendRadius(t.piecePosition.pieceIndex, t.piecePosition.lane.endLaneIndex)
    );
    var a = Track.getBendAngle(t.piecePosition.pieceIndex, t.piecePosition.lane.startLaneIndex);

    var additionalAcc = 0;
    if (r) {
      var MAGIC_CONST_FOR_OUR_RADIUS = Const.MAGIC_CONST_FOR_RADIUS_110 * Math.sqrt(110 / r);
      additionalAcc = MAGIC_CONST_FOR_OUR_RADIUS * t.speed * t.speed + Const.MAGIC_CONST_MAYBE_FRICTION * t.speed;
      if(additionalAcc < 0) additionalAcc = 0; // Пашка - какашка :P
    }
    return Const.MAGIC_CONST_ALPHA * t.angle * t.speed + Const.MAGIC_CONST_BETA * t.angleSpeed + additionalAcc * Other.sign(a);
  }

  /**
   * Возвращает Throttle, который надо нажать, чтобы в кратчайшие сроки достигнуть desiredSpeed
   * @param desiredSpeed
   * @returns int
   */
  this.getThrottle = function (desiredSpeed) {
    if (Const.power === null || Const.airDrag === null)
      return 1; // Пока не знаем физики разгоняемся чтобы её узнать.

    var currentSpeed = this._history[this._maxTick].speed,
      desiredThrottle = (desiredSpeed - (1 + Const.airDrag) * currentSpeed) / Const.power;

    if (Math.abs(currentSpeed - desiredSpeed) < 0.0000000001)
      desiredThrottle = -desiredSpeed * Const.airDrag / Const.power; //
    return Math.max(0, Math.min(1, desiredThrottle)); // throttle ограничен 0 и 1
  }

  /*
   * Возвращает точку, в которую мы попадём через заданное время с заданным положением педали.
   * @param point Начальная точка.
   * @param throttle Положение педали газа.
   * @param ticks Количество тиков, для которого мы хотим получить предсказание. По умолчанию 1.
   * @returns Object
   */
  this.predictPosition = function (point, throttle, ticks) {
    if (typeof ticks === 'undefined') ticks = 1;
    var tmpPoint = {
      piecePosition: {
        pieceIndex: point.piecePosition.pieceIndex,
        inPieceDistance: point.piecePosition.inPieceDistance,
        lane: {
          startLaneIndex: point.piecePosition.lane.startLaneIndex,
          endLaneIndex: point.piecePosition.lane.endLaneIndex
        }
      },
      speed: point.speed,
      acc: point.acc,
      angle: point.angle,
      angleSpeed: point.angleSpeed,
      angleAcc: point.angleAcc
    };

    // Взято из Солвера
    var _switchHistory = []; // История принятых решений о переключениях лейнов.

    /*
     * Было ли переключение лейнов на указанном куске. Работает так же для будущих переключений
     * (для которых команда уже отправлена, но фактического переключения ещё не было).
     * @param index Индекс интересующего куска карты.
     * @returns bool
     */
    var switchedOnPiece = function (index) {
      if (_switchHistory.length < index) return false;
      if (!_switchHistory[index]) return false;
      return true;
    }

    var nextLaneIndex = -1;

    for (var i = 1; i <= ticks; i++) {
      tmpPoint.angleAcc = this.predictAngleAcc(tmpPoint);
      tmpPoint.angleSpeed = tmpPoint.angleSpeed + tmpPoint.angleAcc;
      tmpPoint.angle = tmpPoint.angle + tmpPoint.angleSpeed;

      tmpPoint.acc = this.predictAcc(tmpPoint, throttle);
      tmpPoint.speed += tmpPoint.acc;
      tmpPoint.piecePosition.inPieceDistance += tmpPoint.speed;
      var pieceLength = (Track.getPieceLength(tmpPoint.piecePosition.pieceIndex, tmpPoint.piecePosition.lane.startLaneIndex) +
        Track.getPieceLength(tmpPoint.piecePosition.pieceIndex, tmpPoint.piecePosition.lane.endLaneIndex)) / 2;
      while (tmpPoint.piecePosition.inPieceDistance >= pieceLength) {
        tmpPoint.piecePosition.inPieceDistance -= pieceLength;
        tmpPoint.piecePosition.pieceIndex++;
        if (nextLaneIndex > -1) {
          tmpPoint.piecePosition.lane = {
            startLaneIndex: nextLaneIndex,
            endLaneIndex: nextLaneIndex
          }
          nextLaneIndex = -1;
        }
        while (tmpPoint.piecePosition.pieceIndex >= Track.getPieceCount()) {
          tmpPoint.piecePosition.pieceIndex -= Track.getPieceCount();
          tmpPoint.piecePosition.lap++;
          _switchHistory = [];
        }
      }

      // Если тут свич, то скорее всего мы меняем лейн
      var optLaneIndex = -1,
        curPiece = Track.getPiece(tmpPoint.piecePosition.pieceIndex),
        curLane = tmpPoint.piecePosition.lane.endLaneIndex;

      if (curPiece.switch && !switchedOnPiece(tmpPoint.piecePosition.pieceIndex)) {
        var minLength = 100500;
        [curLane, curLane - 1, curLane + 1].forEach(function (laneIndex) {
          if (laneIndex >= 0 && laneIndex <= Track._track.lanes.length - 1 && curPiece.middleLength[laneIndex] < minLength) {
            minLength = curPiece.middleLength[laneIndex];
            optLaneIndex = laneIndex;
          }
        });
      }

      if (curPiece.switch && !switchedOnPiece(tmpPoint.piecePosition.pieceIndex) && (curLane != optLaneIndex)) {
        // Значит-с на след. писе мы уже будем на другом лейне
        _switchHistory[tmpPoint.piecePosition.pieceIndex] = {
          startLaneIndex: curLane,
          endLaneIndex: optLaneIndex
        };
        nextLaneIndex = optLaneIndex;
      }
    }

    return tmpPoint;
  }
}

module.exports = new Telemetry();
