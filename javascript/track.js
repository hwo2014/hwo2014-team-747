function sign(a) {
  if (a == 0) return 0;
  return a / Math.abs(a);
}

// Различные манипуляции с трассой
function Track() {
  this._track = null;
//  this._middleSwitches=[];
  /**
   * Сохраняем карту, расчитывая сразу длины всех кривых участков
   *
   * @param track
   */
  this.load = function (track) {
    var curSw = -1; // Текуший свитч
    for (i = track.pieces.length - 1; i > 0; i--) {
      if (track.pieces[i].switch) {
        curSw = i; // Последний свитч на трассе ( начальные куски будут учитываются в длине между ним и первым)
        track.pieces[curSw].middleLength = [];
        break;
      }
    }

    track.pieces.forEach(function (piece, pieceIndex) {
      piece.laneLengths = [];
      track.lanes.forEach(function (lane, laneIndex) {
        var pieceLength = 0;

        if (piece.radius) {
          pieceLength = Math.PI * (piece.radius + lane.distanceFromCenter * (piece.angle > 0 ? -1 : 1)) * Math.abs(piece.angle) / 180.0;
        } else {
          pieceLength = piece.length;
        }
        piece.laneLengths.push(pieceLength);
        track.pieces[curSw].middleLength[laneIndex] = (track.pieces[curSw].middleLength[laneIndex] || 0) + pieceLength;
      });
      if (piece.switch) {
        curSw = pieceIndex;
        track.pieces[curSw].middleLength = track.pieces[curSw].middleLength || []; // Инит, если не был ранее задан (для последнего свитча)
      }
    });
    this._track = track;
    //console.log(track.pieces);
  }

  this.getPieceCount = function () {
    return this._track.pieces.length;
  }

  this.getPiece = function (index) {
    return this._track.pieces[index];
  }

  this.getNextPieceIndex = function (index) {
    index++;
    while (index >= this.getPieceCount()) index -= this.getPieceCount();
    return index;
  }

  this.getPieceLength = function (pieceIndex, laneIndex) {
    return this._track.pieces[pieceIndex].laneLengths[laneIndex];
  }

  /**
   * Сравнивает 2 точки
   *
   * @param pointA Объект со свойствами lap, pieceIndex и inPieceDistance
   * @param pointB Объект со свойствами lap, pieceIndex и inPieceDistance
   *
   * @return int Если pointA дальше от старта гонки, чем pointB - вернет 1.
   * Если pointB дальше - вернет -1.
   * Если точки равны - вернет 0.
   */
  this.cmpPoints = function (pointA, pointB) {
    if (pointA.lap > pointB.lap) {
      return 1;
    }
    if (pointA.lap < pointB.lap) {
      return -1;
    }
    if (pointA.pieceIndex > pointB.pieceIndex) {
      return 1;
    }
    if (pointA.inPieceDistance < pointB.inPieceDistance) {
      return -1;
    }
    return 0;
  }

  /**
   * Вычисляет расстояние от начала трассы до точки по лейну, указанному в параметре, без учёта пройденных полных кругов.
   *
   * @param point Объект со свойствами pieceIndex, inPieceDistance и lane
   */
  this.getDistanceSinceLapStart = function (point) {
    var res = 0.0;

    for (var i = 0; i < point.pieceIndex; i++) {
      res += this.getPieceLength(i, point.lane.startLaneIndex);
    }

    res += point.inPieceDistance;

    return res;
  }

  /**
   * Вычисляет расстояние от начала трассы до точки по лейну, указанному в параметре, с учётом пройденных полных кругов.
   *
   * @param point Объект со свойствами lap, pieceIndex, inPieceDistance и lane
   */
  this.getDistanceSinceGameStart = function (point) {
    var res = 0.0;

    for (var i = 0; i < point.lap; i++) {
      res += this.getLapLength(point.lane.startLaneIndex);
    }

    res += this.getDistanceSinceLapStart(point);

    return res;
  }

  /**
   * Вычисляет длину целового круга при движении по указанному лейну.
   *
   * @param laneIndex Лейн, для которого необходимо посчитать длину круга.
   */
  this.getLapLength = function (laneIndex) {
    var res = 0.0;

    for (var i = 0; i < this._track.pieces.length; i++) {
      res += this.getPieceLength(i, laneIndex);
    }

    return res;
  }

  /**
   * Вычисляет расстояние между 2мя точками
   * todo: Пока работает только для соседних тиков, а надо сделать чтобы работало с любыми точками.
   *
   * @param pointA Объект со свойствами lap, pieceIndex, inPieceDistance и lane
   * @param pointB Объект со свойствами lap, pieceIndex, inPieceDistance и lane
   */
  this.getDistanceBetweenPoints = function (pointA, pointB) {
    return this.getDistanceSinceGameStart(pointB) - this.getDistanceSinceGameStart(pointA);
  }

  /**
   * Возращает радиус бэнда в текущей точке (для прямой вернет false)
   *
   * @param pieceIndex
   * @param laneIndex
   * @return int|false
   */
  this.getBendRadius = function (pieceIndex, laneIndex) {
    var r = this._track.pieces[pieceIndex].radius;
    if (r) {
      return r - this._track.lanes[laneIndex].distanceFromCenter * sign(this._track.pieces[pieceIndex].angle);
    } else {
      return false;
    }
  }

  this.getBendAngle = function (pieceIndex, laneIndex) {
    var r = this._track.pieces[pieceIndex].angle;
    if (r) {
      return r;
    } else {
      return false;
    }
  }

}

module.exports = new Track();
