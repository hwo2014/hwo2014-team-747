var Telemetry = require('./telemetry');
var Track = require('./track');
var Const = require('./const');

function Solver() {
  this._switchHistory = []; // История принятых решений о переключениях лейнов.

  /*
   * Было ли переключение лейнов на указанном куске. Работает так же для будущих переключений
   * (для которых команда уже отправлена, но фактического переключения ещё не было).
   * @param index Индекс интересующего куска карты.
   * @returns bool
   */
  this.switchedOnPiece = function (index) {
    if (this._switchHistory.length < index) return false;
    if (!this._switchHistory[index]) return false;
    return true;
  }

  /**
   * Главная, Богоподобная, Солнцеликая функция, пробабушка Великой и Ужасной Ванги.
   */
  this.get = function () {
    var tick = Telemetry.getCurrentTick();
    var p = Telemetry.getCurrentPosition();
    var nextPieceIndex = Track.getNextPieceIndex(p.piecePosition.pieceIndex);
    var nextPiece = Track.getPiece(nextPieceIndex);

    if (p.prev && (p.prev.piecePosition.lap < p.piecePosition.lap)) {
      this._switchHistory = [];
    }

    var optLaneIndex = 0,
      curLane = p.piecePosition.lane.endLaneIndex;
    if (nextPiece.switch && !this.switchedOnPiece(nextPieceIndex)) {
      var minLength = 100500;
      [curLane, curLane - 1, curLane + 1].forEach(function (laneIndex) {
        if (laneIndex >= 0 && laneIndex <= Track._track.lanes.length - 1 && nextPiece.middleLength[laneIndex] < minLength) {
          minLength = nextPiece.middleLength[laneIndex];
          optLaneIndex = laneIndex;
        }
      });
    }

    if (Const._initialized && nextPiece.switch && !this.switchedOnPiece(nextPieceIndex) && (curLane != optLaneIndex) && !(Telemetry._turbo && Telemetry._turbo.RightNow) && p.speed < 12) {
      var dir = (curLane > optLaneIndex) ? "Left" : "Right";
      this._switchHistory[nextPieceIndex] = {
        startLaneIndex: curLane,
        endLaneIndex: optLaneIndex
      };
      console.log(this._switchHistory[nextPieceIndex]);
      Telemetry.registerThrottle();
      console.log('SW: ' + dir);
      return {
        msgType: "switchLane",
        data: dir,
        gameTick: tick
      };
    } else if (tick) {
      var t = 1.0,
        i;

      function isOk(p, t, n) {
        var j;
        for (j = 0; j < n; j++) {
          var a = Telemetry.predictPosition(p, t, j).angle;
          if (Math.abs(a) > 50) {
            //console.log('predicted Problem on tick', j, a);
            return false;
          }
        }
        return true;
      }

      for (i = 20; i >= 0; i--) { // Подбираем скорость

        t = i / 20;
        if (isOk(Telemetry.predictPosition(p, t), 0.0, (p.speed * 6) | 0))
          break;
      }

      if (Telemetry._turbo && !Telemetry._turbo.RightNow && t == p.prev.throttle && t == 1) {// Есть порох и сейчас не нужно управлять
        // Турбу надо включать там где сможем проехать хотябы 10 тиков с ней
        if (isOk(Telemetry.predictPosition(p, Telemetry._turbo.turboFactor, 13), 0.0, (p.speed * Telemetry._turbo.turboFactor * 2.1) | 0)) { // Если можно
          // то, хуячим
          Telemetry._turbo.RightNow = true;
          Telemetry.registerThrottle(t);
          Telemetry.downTurbo();
          console.log('# WE started using turbo!');
          return {
            msgType: "turbo",
            data: "Run forest, run, run just 4 fun",
            gameTick: tick
          };
        }
      }
      if (Telemetry._turbo.RightNow)
        Telemetry.downTurbo();


      for (i = 20; i >= 0; i--) { // Подбираем скорость
        var factor=(Telemetry._turbo&& Telemetry._turbo.RightNow)?Telemetry._turbo.turboFactor:1.0;

        t = i / 20;
        if (isOk(Telemetry.predictPosition(p, t * factor), 0.0, (p.speed * 6) | 0))
          break;
      }
      //t = (isOk(Telemetry.predictPosition(p, t), 0.0, (p.speed * 6) | 0)) ? 1 : 0;

      Telemetry.registerThrottle(t);
      console.log('TR: ' + t);
      return {
        msgType: "throttle",
        data: t,
        gameTick: tick
      };
    }
  }
}

module.exports = new Solver();
