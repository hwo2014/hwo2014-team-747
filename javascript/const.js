var Other = require('./other');
var SystemSolver = require('./system_solver');
var Track = require('./track');
// В  се что свяазано с физ. константами
function Const() {
  this.power = null; // Описывает ускорение acc (t+1) = throttle (t) * power - speed (t) * airDrag
  this.airDrag = null;

  this.dimensions = {length: null, width: null, guideFlagPosition: null};
  this._initialized = false;
  this.ourCar = {name: null, color: null};

  this.MAGIC_CONST_MAYBE_FRICTION = -0.3;
  this.MAGIC_CONST_FOR_RADIUS_110 = 0.050564989684768;
  this.MAGIC_CONST_ALPHA = -0.00125;
  this.MAGIC_CONST_BETA = -0.1;

  this.tryToObtain = function (history, gameTick) {
    if (this._initialized) return true;

    if (this.power == null) { // Getting power value
      this.power = history[gameTick].acc; // == .speed, первый тик всегда жмем 1.0
    }
    else if (this.airDrag == null) {
      this.airDrag = (history[gameTick].acc - 1.0 * this.power) / history[gameTick - 1].speed; //1.0, т.к. второй тик всегда жмем 1.0
      this.airDrag = Math.round(this.airDrag * 10000000) / 10000000;
      if (Other.useColors) {
        console.log('Basic physics detected! power: '.red + this.power.toString().blue + '; airDrag: '.red + this.airDrag.toString().blue);
      } else {
        console.log('Basic physics detected! power: ' + this.power + '; airDrag: ' + this.airDrag);
      }
    } else if (history[gameTick].angleAcc) {
      var p = history[gameTick - 1];
      var r = Math.min(
        Track.getBendRadius(p.piecePosition.pieceIndex, p.piecePosition.lane.startLaneIndex),
        Track.getBendRadius(p.piecePosition.pieceIndex, p.piecePosition.lane.endLaneIndex)
      );
      var a = Track.getBendAngle(p.piecePosition.pieceIndex, p.piecePosition.lane.startLaneIndex);
      var consts = SystemSolver.add(p.speed, p.angleSpeed, p.angle, r, a, history[gameTick].angleAcc);
      if (consts !== true) {
        if (typeof consts[0] != 'undefined') {
          this.MAGIC_CONST_ALPHA = consts[0];
          this.MAGIC_CONST_BETA = consts[1];
          this.MAGIC_CONST_FOR_RADIUS_110 = consts[2];
          this.MAGIC_CONST_MAYBE_FRICTION = consts[3];
        }
        this._initialized = true;
      }
    }
  };

  this.reset = function () {
    this.power = null; // Описывает ускорение acc (t+1) = throttle (t) * power - speed (t) * airDrag
    this.airDrag = null;
    this.dimensions = {length: null, width: null, guideFlagPosition: null};
    this._initialized = false;
  }

  this.fillDimensions = function (data) {
    this.dimensions = data.dimensions;
    if (Other.useColors) {
      console.log('Got dimensions! length: '.red + this.dimensions.length.toString().blue + '; width: '.red + this.dimensions.width.toString().blue + '; guideFlagPosition: '.red + this.dimensions.guideFlagPosition.toString().blue);
    } else {
      console.log('Got dimensions! length: ' + this.dimensions.length + '; width: ' + this.dimensions.width + '; guideFlagPosition: ' + this.dimensions.guideFlagPosition);
    }
  };

  this.saveInfoAboutOurCarBotDriverNameAndColorToPublicObjectOfConstJs = function (data) {
    this.ourCar = data;
    if (Other.useColors) {
      console.log('Got our car info! name: '.red + this.ourCar.name.blue + '; color: '.red + this.ourCar.color.blue);
    } else {
      console.log('Got our car info! name: ' + this.ourCar.name + '; color: ' + this.ourCar.color);
    }
  }
}

module.exports = new Const();