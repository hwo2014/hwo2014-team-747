var Other = require('./other');
var Const = require('./const');
// Решатель уравнений
function SystemSolver() {
  this._data = [];

  // Собираем данные
  this.add = function (speed, angleSpeed, angle, r, a, angleAccOnNextTick) {
    // console.log(r, a);
    // MAGIC_CONST_ALPHA * angle * speed + MAGIC_CONST_BETA * angleSpeed
    // + MAGIC_CONST_FOR_OUR_RADIUS * speed * speed + MAGIC_CONST_MAYBE_FRICTION * speed
    this._data.push([angle * speed, angleSpeed, speed * speed * Math.sqrt(110 / r) * Other.sign(a), speed * Other.sign(a), angleAccOnNextTick]);
    if (this._data.length == 4) {
      // Находим физику
      console.log(this._data);
      var consts = this.solve(this._data);
      console.log(consts);
      return consts;
    } else {
      return true;
    }
  }

  this.reduce = function (n, d) {
    if (d == 0) return;
    return n / d;
  }

  this.solve = function (matrix) {
    // Cramer's Rule is used to solve this system.
    var w1 = matrix[0][0], x1 = matrix[0][1], y1 = matrix[0][2], z1 = matrix[0][3], s1 = matrix[0][4];
    var w2 = matrix[1][0], x2 = matrix[1][1], y2 = matrix[1][2], z2 = matrix[1][3], s2 = matrix[1][4];
    var w3 = matrix[2][0], x3 = matrix[2][1], y3 = matrix[2][2], z3 = matrix[2][3], s3 = matrix[2][4];
    var w4 = matrix[3][0], x4 = matrix[3][1], y4 = matrix[3][2], z4 = matrix[3][3], s4 = matrix[3][4];

    var Mw, Mx, My, Mz, Ms, D, Dw, Dx, Dy, Dz, w, x, y, z;
    Mw = x2 * (y3 * z4 - z3 * y4) - y2 * (x3 * z4 - z3 * x4) + z2 * (x3 * y4 - y3 * x4);
    Mx = w2 * (y3 * z4 - z3 * y4) - y2 * (w3 * z4 - z3 * w4) + z2 * (w3 * y4 - y3 * w4);
    My = w2 * (x3 * z4 - z3 * x4) - x2 * (w3 * z4 - z3 * w4) + z2 * (w3 * x4 - x3 * w4);
    Mz = w2 * (x3 * y4 - y3 * x4) - x2 * (w3 * y4 - y3 * w4) + y2 * (w3 * x4 - x3 * w4);
    D = w1 * Mw - x1 * Mx + y1 * My - z1 * Mz;

    Ms = x2 * (y3 * z4 - z3 * y4) - y2 * (x3 * z4 - z3 * x4) + z2 * (x3 * y4 - y3 * x4);
    Mx = s2 * (y3 * z4 - z3 * y4) - y2 * (s3 * z4 - z3 * s4) + z2 * (s3 * y4 - y3 * s4);
    My = s2 * (x3 * z4 - z3 * x4) - x2 * (s3 * z4 - z3 * s4) + z2 * (s3 * x4 - x3 * s4);
    Mz = s2 * (x3 * y4 - y3 * x4) - x2 * (s3 * y4 - y3 * s4) + y2 * (s3 * x4 - x3 * s4);
    Dw = s1 * Ms - x1 * Mx + y1 * My - z1 * Mz;

    Mw = s2 * (y3 * z4 - z3 * y4) - y2 * (s3 * z4 - z3 * s4) + z2 * (s3 * y4 - y3 * s4);
    Ms = w2 * (y3 * z4 - z3 * y4) - y2 * (w3 * z4 - z3 * w4) + z2 * (w3 * y4 - y3 * w4);
    My = w2 * (s3 * z4 - z3 * s4) - s2 * (w3 * z4 - z3 * w4) + z2 * (w3 * s4 - s3 * w4);
    Mz = w2 * (s3 * y4 - y3 * s4) - s2 * (w3 * y4 - y3 * w4) + y2 * (w3 * s4 - s3 * w4);
    Dx = w1 * Mw - s1 * Ms + y1 * My - z1 * Mz;

    Mw = x2 * (s3 * z4 - z3 * s4) - s2 * (x3 * z4 - z3 * x4) + z2 * (x3 * s4 - s3 * x4);
    Mx = w2 * (s3 * z4 - z3 * s4) - s2 * (w3 * z4 - z3 * w4) + z2 * (w3 * s4 - s3 * w4);
    Ms = w2 * (x3 * z4 - z3 * x4) - x2 * (w3 * z4 - z3 * w4) + z2 * (w3 * x4 - x3 * w4);
    Mz = w2 * (x3 * s4 - s3 * x4) - x2 * (w3 * s4 - s3 * w4) + s2 * (w3 * x4 - x3 * w4);
    Dy = w1 * Mw - x1 * Mx + s1 * Ms - z1 * Mz;

    Mw = x2 * (y3 * s4 - s3 * y4) - y2 * (x3 * s4 - s3 * x4) + s2 * (x3 * y4 - y3 * x4);
    Mx = w2 * (y3 * s4 - s3 * y4) - y2 * (w3 * s4 - s3 * w4) + s2 * (w3 * y4 - y3 * w4);
    My = w2 * (x3 * s4 - s3 * x4) - x2 * (w3 * s4 - s3 * w4) + s2 * (w3 * x4 - x3 * w4);
    Ms = w2 * (x3 * y4 - y3 * x4) - x2 * (w3 * y4 - y3 * w4) + y2 * (w3 * x4 - x3 * w4);
    Dz = w1 * Mw - x1 * Mx + y1 * My - s1 * Ms;

    w = this.reduce(Dw, D);
    x = this.reduce(Dx, D);
    y = this.reduce(Dy, D);
    z = this.reduce(Dz, D);
    return [w, x, y, z];
  }
}

module.exports = new SystemSolver();