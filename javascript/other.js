function Other() {
  this.useColors = false;

  this.padLeft = function (input, len, str) {
    return Array(len - String(input).length + 1).join(str || '0') + input;
  };

  this.sign = function (a) {
    if (a == 0) return 0;
    return a / Math.abs(a);
  }
}

module.exports = new Other();
