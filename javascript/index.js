var colors = require('colors');
var net = require("net");
var JSONStream = require('JSONStream');
var Track = require('./track');
var Telemetry = require('./telemetry');
var Logger = require('./logger');
var Const = require('./const');
var Solver = require('./solver');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function () {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
}

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function (data) {
  console.log('GOT: msgType - ' + data.msgType);
  if (data.msgType === 'carPositions') {
    Logger.start(['piecePosition.pieceIndex', 'piecePosition.inPieceDistance', 'speed', 'angleSpeed', 'angle', 'angleAcc', 'next.angle', 'next.angleAcc'], false);

    Telemetry.up(data.data, data.gameTick | 0);

    if (!data.gameTick)
      return;

    var solution = Solver.get();
    if (solution) {
      send(solution);
    }

  } else {
    if (data.msgType === 'join') {
      console.log('Joined');
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
      send({
        msgType: "throttle",
        data: 1
      });
    } else if (data.msgType === 'yourCar') {
      Const.saveInfoAboutOurCarBotDriverNameAndColorToPublicObjectOfConstJs(data.data);
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (data.msgType === 'turboAvailable') {
      Telemetry.upTurbo(data.data);
      console.log(data);
    } else if (data.msgType === 'lapFinished') {
      console.log(data);
    } else if (data.msgType === 'crash') {
      console.log(data);
    } else if (data.msgType === 'gameInit') {
      Track.load(data.data.race.track);
      Const.fillDimensions(data.data.race.cars[0]); // todo: detect Our ID!
      //Const.reset();
      Telemetry.init();
    }
  }
});

jsonStream.on('error', function () {
  return console.log("disconnected");
});
